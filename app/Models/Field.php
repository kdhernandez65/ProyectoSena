<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $table = 'fields';
    protected $fillable = ['status_id','name','foto'];
    protected $guarded = ['id'];

    public function rentals()
    {
    	return $this->hasMany('App\Models\Rental');
    }

    public function status()
    {
    	return $this->belongsTo('App\Models\Status');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rental extends Model
{
    protected $table = 'rentals';
    protected $fillable = ['user_id', 
    'status_id', 
    'field_id', 
    'date',
    'hora_inicio',
    'hora_fin'];
    protected $guarded = ['id'];

    public function field()
    {
    	return $this->belongsTo('App\Models\Field');
    }


    public function user()
    {
    	return $this->belongsTo('App\User');	
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

}


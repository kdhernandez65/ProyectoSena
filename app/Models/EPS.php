<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EPS extends Model
{
    protected $table = 'e_p_s';
    protected $fillable = ['name','phone'];
    protected $guarded = ['id'];


   public function users()
    {
    	return $this->hasMany('App\User');
    }
}

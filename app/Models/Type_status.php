<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_status extends Model
{
    protected $table = 'Type_statuses';
    protected $fillable = ['name'];
    protected $guarded = ['id'];

    public function statuses()
    {
    	return $this->hasMany('App\Models\Status');
    }
}

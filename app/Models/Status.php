<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';
    protected $fillable = ['name', 'description'];
    protected $guarded = ['id'];


    //relacion Uno A muchos Status->user

    public function users()
    {
    	return $this->hasMany('App\User');	
    }


    public function type_status()
    {
    	return $this->belongsTo('App\Models\Type_status');
    }

    public function fields()
    {
    	return $this->hasMany('App\Models\Field');	
    }


    public function rentals()
    {
        return $this->hasMany('App\Models\Rental');  
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name'];
    protected $guarded = ['id'];


    //relacion uno a muchos Role-> usuario

    public function users()
    {
    	return $this->hasMany('App\User');
    }
}

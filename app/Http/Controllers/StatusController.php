<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Status as Status;
use App\Models\Type_status;

class StatusController extends Controller
{
    public function index()
    {
    	$statuses = Status::all();
        $type_statuses = Type_status::all();
    	return \View::make('statuses/list',compact('statuses','type_statuses'));
    }

    public function edit($id)
    {
    	$status = Status::find($id);
        $type_statuses = Type_status::all();
    	return \View::make('statuses/update',compact('status','type_statuses'));
    }

    public function update($id, Request $request)
    {
    	$status = Status::find($id);
    	$status->name = $request->name;
    	$status->description = $request->description;
        $status->type_status_id = $request->type_status_id;
    	$status->save();
    	return redirect(\Auth::user()->urlStatusAll());
    }

    public function show(Request $request)
    {
    	$statuses = Status::where('name','like','%'.$request->name.'%')->get();
        $type_statuses = Type_status::all();
    	return \View::make('statuses/list',compact('statuses','type_statuses'));
    }

    public function destroy($idEstado)
    {
    	$status = Status::find($idEstado);
    	$status->delete();
    	return redirect()->back();
    }

	public function create()
    {
        $type_statuses = Type_status::all();
        return \View::make('statuses/list',compact('type_statuses'));
    }


    public function store(Request $request)
    {
    	$status = new Status;
    	$status->name = $request->name;
        $status->description = $request->description;
        $status->type_status_id = $request->type_status_id;
        $status->save();
    	return redirect(\Auth::user()->urlStatusAll());
    }
}

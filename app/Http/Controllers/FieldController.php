<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Field as Field;
use App\Models\Status;

class FieldController extends Controller
{
    public function index()
    {
        $fields = Field::all();
        $statuses = Status::where('type_status_id',1)->get();
        return \View::make('fields/list',compact('fields','statuses'));
    }

    public function create()
    {   
        $statuses = Status::where('type_status_id',1)->get();
        return \View::make('fields/new',compact('statuses'));
    }
    
    public function edit($id)
    {
        $field = Field::find($id);
        $statuses = Status::where('type_status_id', 1)->get();
        return \View::make('fields/update',compact('field','statuses'));
    }
    public function update($id, Request $request)
    {
        $field = Field::find($id);
        $field->status_id = $request->status_id;
        $field->name = $request->name;
        $fileName               = $request->name.'.'.$request->file('foto')->extension();
            $path                   = '/public/images/';
            $storePath              = '/storage/images/';
            $field->foto = $storePath.$fileName;
            $request->file('foto')->storeAs($path, $fileName);
        $field->save();
        return redirect(\Auth::user()->urlFieldAll());
    }

    public function show(Request $request)
    {
        $fields = Field::where('name','like','%'.$request->name.'%')->get();
        $statuses = Status::where('type_status_id', 1)->get();
        return \View::make('fields/list',compact('fields','statuses'));
    }

    public function destroy($id)
    {
        $field = Field::find($id);
        $field->delete();
        return redirect()->back();
    }

    public function store(Request $request)
    {

        $field = new Field;
        $field->status_id = $request->status_id;
        $field->name = $request->name;
        $fileName               = $request->name.'.'.$request->file('foto')->extension();
            $path                   = '/public/images/';
            $storePath              = '/storage/images/';
            $field->foto = $storePath.$fileName;
            $request->file('foto')->storeAs($path, $fileName);
        $field->save();
        return redirect(\Auth::user()->urlFieldAll());
    }

}

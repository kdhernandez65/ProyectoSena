<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rental as Rental;
use App\Models\Field;
use App\Models\Status;
use App\User;


class ReservaController extends Controller
{
    public function index()
    {
        if (\Auth::user()->role_id == 3) {
           $rentals = Rental::where('user_id', \Auth::user()->id)->get();
        }else{
    	   $rentals = Rental::all();
        }
    	$fields = Field::all();
        return \View::make('rentals/reservas',compact('fields','rentals'));
    }

    public function edit($id)
    {
    	$user = User::all();        
    	$statuses = Status::where('type_status_id', 2)->get();
        $fields = Field::all();
        $rental = Rental::find($id);
    	return \View::make('rentals/update',compact('user','statuses','rental','fields'));
    }

    public function update($id, Request $request)
	{
		$rental = Rental::find($id);
		$rental->status_id = $request->status_id;
		$rental->field_id = $request->field_id;
		$rental->price_id = 1;
		$rental->date = $request->date;
		$rental->hora_inicio = $request->hora_inicio;
		$rental->hora_fin = $request->hora_fin;
		$rental->save();
		return redirect(\Auth::user()->urlReservasAll());
	}


	public function destroy($id)
    {
    	$rental = Rental::find($id);
    	$rental->delete();
    	return redirect()->back();
    }
}

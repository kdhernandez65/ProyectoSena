<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EPS as EPS;

class EpsController extends Controller
{
   public function index()
    {
        $epeses = EPS::all();
        return \View::make('epeses/list',compact('epeses'));
    }

    public function create()
    {
        return \View::make('epeses/new');
    }
    
    public function edit($id)
    {
        $eps = EPS::find($id);
        return \View::make('epeses/update',compact('eps'));
    }
    public function update($id, Request $request)
    {
        $eps = EPS::find($id);
        $eps->name = $request->name;
        $eps->phone = $request->phone;
        $eps->save();
        return redirect(\Auth::user()->urlEpsAll());
    }

    public function show(Request $request)
    {
        $epeses = EPS::where('name','like','%'.$request->name.'%')->get();
        return \View::make('epeses/list',compact('epeses'));
    }

    public function destroy($id)
    {
        $eps = EPS::find($id);
        $eps->delete();
        return redirect()->back();
    }

    public function store(Request $request)
    {
        $eps = new EPS;
        $eps->create($request->all());
        return redirect(\Auth::user()->urlEpsAll());
    }


}

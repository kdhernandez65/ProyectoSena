<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Status;
use App\Models\{Role,EPS};
use Illuminate\Support\Facades\Mail;
use App\Mail\CrearUsuario;


class UserController extends Controller
{
    public function index()
    {
    	$users = User::all();
        $epeses = EPS::all();
    	return \View::make('users/list',compact('users','epeses'));
    }

    public function edit($id)
    {
    	$user = User::find($id);
        $roles = Role::all();
        $statuses = Status::where('type_status_id',3)->get();
        $epeses = EPS::all();
    	return \View::make('users/update',compact('user','roles','statuses','epeses'));
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        $user->status_id = $request->status_id;
        $user->eps_id = $request->eps_id;
        $user->role_id = $request->role_id;
        $user->name        = $request->name;
        $user->lastname = $request->lastname;
        $user->phone = $request->phone;
        $user->document = $request->document;
        $user->date = $request->date;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->status_id = $request->status_id;
        $user->eps_id = $request->eps_id;
        $user->role_id = $request->role_id;
        
        $user->save();
        return redirect(\Auth::user()->urlUserAll());
    }


    

    public function show(Request $request)
    {
    	$users = User::where('name','like','%'.$request->nombre.'%')->get();
        $epeses = EPS::all();
    	return \View::make('users/list',compact('users','epeses'));
    }

    public function destroy($id)
    {
    	$user = User::find($id);
    	$user->delete();
    	return redirect()->back();
    }

	public function create()
	{   
        $epeses = EPS::all();
		return \View::make('users/list',compact('epeses'));
	}

    public function store(Request $request)
    {
    	$user = new User;
        $user->name        = $request->name;
        $user->lastname = $request->lastname;
        $user->phone = $request->phone;
        $user->document = $request->document;
        $user->date = $request->date;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->password = \Hash::make('amazu123');
        $user->status_id = 1;
        $user->eps_id = $request->eps_id;
        $user->role_id = 2;
        $user->save();

        Mail::to($request->email)->send(new CrearUsuario($user));
        return redirect(\Auth::user()->urlUserAll());
    }
}

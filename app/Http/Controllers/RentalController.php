<?php

namespace App\Http\Controllers;
use App\Models\Rental as Rental;
use App\Models\Field;
use App\User;
use App\Models\Status;
use Illuminate\Http\Request;


class RentalController extends Controller
{
    public function index()
    {
        $fields = Field::all();
        $statuses = Status::where('id', 6)->get();
        return \View::make('rentals/reservar',compact('fields','statuses'));
    }


    public function create()
    {   
        return \View::make('rentals/reservar');
    }

    public function store(Request $request)
    {
        $rental = new Rental;
        $rental->user_id = \Auth::user()->id;
        $rental->status_id = 4;
        $rental->field_id =$request->field_id;
        $rental->price_id = 1;
        $rental->date = $request->date;
        $rental->hora_inicio = $request->hora_inicio;
        $rental->hora_fin = $request->hora_fin;
        $rental->save();
        return redirect(\Auth::user()->urlReservasAll());
    }

}

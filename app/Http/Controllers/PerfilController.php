<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class PerfilController extends Controller
{

    public function edit($id)
    {
        $user = User::find($id);
        return \View::make('users/updateperfil',compact('user'));
    }

    public function update($id, Request $request)
    {
    	$user =  User::find($id);
        $user->name        = $request->name;
        $user->lastname = $request->lastname;
        $user->phone = $request->phone;
        $user->document = $request->document;
        $user->date = $request->date;
        $user->address = $request->address;
        $user->email = $request->email;
        $user->password = \Hash::make($request->password);
        $user->status_id = 1;
        $user->eps_id = 1;
        $user->role_id = \Auth::user()->role_id;
        $user->save();
        return redirect(\Auth::user()->urlUserHome());
    }
}

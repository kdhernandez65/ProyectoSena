<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status_id','eps_id','role_id','name','lastname', 'phone','document','date','address','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    //relacion muchos a uno 

    public function status()
    {
        return $this->belongsTo('App\Models\Status');
    }

    public function rentals()
    {
        return $this->hasMany('App\Models\Rental');
    }


    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function eps()
    {
        return $this->belongsTo('App\Models\EPS');
    }


    //roles

    public function urlRoleAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/role';
    }

    public function urlRoleCreate()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/role/create';
    }

    public function urlRoleShow()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/role/show';
    }

    public function urlRoleEdit($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/role/'.$id.'/edit';
        
    }

    public function urlRoleUpdate($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/role/update/'.$id;
        
    }

    public function urlRoleDestroy($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/role/destroy/'.$id;
        
    }

    //Estados

    public function urlStatusAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/status';
    }

    public function urlStatusCreate()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/status/create';
    }

    public function urlStatusShow()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/status/show';
    }

    public function urlStatusEdit($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/status/'.$id.'/edit';
        
    }

    public function urlStatusUpdate($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/status/update/'.$id;
        
    }

    public function urlStatusDestroy($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/status/destroy/'.$id;
        
    }

    //usuarios

    public function urlUserAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/user';
    }

    public function urlUserCreate()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/user/create';
    }

    public function urlUserShow()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/user/show';
    }

    public function urlUserEdit($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/user/'.$id.'/edit';
    }

    public function urlUserUpdate($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/user/update/'.$id;
    }

    //perfil

    public function urlUserEditPerfil($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/perfil/'.$id.'/edit';
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/perfil/'.$id.'/edit';
        if(\Auth::user()->role->name === 'Cliente')
            return 'client/perfil/'.$id.'/edit';
    }

    public function urlUserUpdatePerfil($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/perfil/update/'.$id;
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/perfil/update/'.$id;
        if(\Auth::user()->role->name === 'Cliente')
            return 'client/perfil/update/'.$id;
        
    }

    public function urlUserHome()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'home';
        if(\Auth::user()->role->name === 'Administrador')
            return 'home';
        if(\Auth::user()->role->name === 'Cliente')
            return 'home';
    }

    public function urlUserDestroy($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/user/destroy/'.$id;
        
    }


    //Canchas

    public function urlFieldAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/field';
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/field';
        if(\Auth::user()->role->name === 'Cliente')
            return 'client/field';
    }

    public function urlFieldCreate()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/field/create';
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/field';
   
    }

    public function urlFieldShow()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/field/show';
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/field/show';
        if(\Auth::user()->role->name === 'Cliente')
            return 'client/field/show';
    }

    public function urlFieldEdit($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/field/'.$id.'/edit';
          if(\Auth::user()->role->name === 'Administrador')
            return 'admin/field/'.$id.'/edit';
        
    }

    public function urlFieldUpdate($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/field/update/'.$id;
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/field/update/'.$id;
        
    }

    public function urlFieldDestroy($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/field/destroy/'.$id;
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/field';
        
    }
    //reservas

    public function urlReservasAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/reserva';
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/reserva';
        if(\Auth::user()->role->name === 'Cliente')
            return 'client/reserva';
    }


    public function urlReservasEdit($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/reserva/'.$id.'/edit';
          if(\Auth::user()->role->name === 'Administrador')
            return 'admin/reserva/'.$id.'/edit';
        
    }

    public function urlReservasUpdate($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/reserva/update/'.$id;
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/reserva/update/'.$id;
        
    }

    public function urlReservasDestroy($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/reserva/destroy/'.$id;
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/reserva';
        
    }



    // rental
    public function urlRentalAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/rental';
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/rental';
        if(\Auth::user()->role->name === 'Cliente')
            return 'client/rental';
    }

    public function urlRentalCreate()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/rental/create';
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/rental';
        if(\Auth::user()->role->name === 'Cliente')
            return 'client/rental';
   
    }

    public function urlRentalShow()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/rental/show';
        if(\Auth::user()->role->name === 'Administrador')
            return 'admin/rental/show';
        
    }

    

    //eps
    public function urlEpsAll()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/eps';
    }

    public function urlEpsCreate()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/eps/create';
    }

    public function urlEpsShow()
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/eps/show';
    }

    public function urlEpsEdit($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/eps/'.$id.'/edit';
        
    }

    public function urlEpsUpdate($id)
    {
        if(\Auth::user()->role->name === 'Gerente')
            return 'Manager/eps/update/'.$id;
        
    }


}

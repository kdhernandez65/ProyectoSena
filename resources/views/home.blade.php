@extends('layouts.argon')

@section('content')
<br><br><br><br><div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-secondary"><h1 >Bienvenido {{ Auth::user()->role->name}}</h1></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h2>{{ __('You are logged in!') }}</h2>
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

@extends('layouts.argon')
@section('content')
	<section class="main-content" id="panel">
        <div class="header  pb-6">
            <div class="container-fluid ">
                <div class="header-body " >
                    <div class="row align-items-right py-4">
                        <div class="col-lg-6 col-7">
                          <h6 class="h2 text-blck d-inline-block ">Reservas</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4  ">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark ">
                              <li class="breadcrumb-item "><a href="{{route('home')}}"><i class="fas fa-home"></i></a></li>
                              <li class="breadcrumb-item "><a href="#">Listado</a></li>
                              <li class="breadcrumb-item active " aria-current="page">Editar</li>
                            </ol>
                          </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid mt--6 ">
            <div class="row ">
                <article class="col-md-12 mt-3">
            		<div class="card border-success">
              			<div class="card-header bg-success text-white">
                			Informacion Reserva
              			</div>
              			<div class="card-body ">
                			<form action="{{url( \Auth::user()->urlReservasUpdate($rental->id) ) }}" method="post">
                                @csrf
                                <div class="container">
                                    
                                        <div class="row">
                                            <div class="col">
                                                <label>Fecha</label>
                                                <input  type="date" name="date" class="form-control" value="{{$rental->date}}">
                                            </div>
                                            <div class="col">
                                                <label >Hora inicio</label>
                                                <input  type="time" name="hora_inicio"class="form-control"  value="{{$rental->hora_inicio}}" >
                                            </div>
                                            <div class="col">
                                                <label >Hora fin</label>
                                                <input  type="time" name="hora_fin"class="form-control"  value="{{$rental->hora_fin}}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <label>Cancha</label>
                                                <select name="field_id" class="form-control" required>
                                                <option value="">Seleccione...</option>
                                                @foreach($fields as $field)
                                                    @if($field->id == $rental->field_id)
                                                        <option value="{{$field->id}}" selected>{{$field->name}}</option>
                                                    @else
                                                        <option value="{{$field->id}}">{{$field->name}}</option>
                                                    @endif
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label class="col">Estado</label>
                                                <select name="status_id" class="form-control" required>
                                                <option value="">Seleccione...</option>
                                                @foreach($statuses as $status)
                                                    @if($status->id == $rental->status_id)
                                                        <option value="{{$status->id}}" selected>{{$status->name}}</option>
                                                    @else
                                                        <option value="{{$status->id}}">{{$status->name}}</option>
                                                    @endif
                                                @endforeach
                                                </select>
                                            </div>
                                        </div><br>
                                        
                                    
                                    <div class="form-group">                                    
                                            <button type="submit" class="btn btn-success float-right ">Actualizar</button>
                                    </div> 
                                </div> 
                			</form>
              			</div>
            		</div>
                </article> 
            </div>
        </div>
	</section>
@endsection
@extends('layouts.argon')
@section('content')
<script>

        (function() {
          'use strict';
          window.addEventListener('load', function() {
            
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
</script>
	<div class="main-content" id="panel">
    <div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-blck d-inline-block mb-0">Reservar</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Listado</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Gestión</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <div>
                <!--<h2 class="mb-0">Gestion Usuarios</h3>-->
              </div><br>
              <!--<form action="{{ url( Auth::user()->urlFieldShow() ) }}" method="post" novalidate class="form-inline">
                   @csrf
                    <div class="form-group">
                        <label class="mr-2">Nombre </label>
                        <input type="text" name="name" class="form-control" placeholder="Buscar por Nombre">
                    </div>
                    <div class="form-group">
                       	<button type="submit" class="btn btn-info ml-2 mr-2">Buscar</button>
                        <a href="{{ url( Auth::user()->urlFieldAll()) }}" class="btn btn-warning">Todo</a>
                        @if( Auth::user()->role->name != 'Cliente')
                          <a href="{{ url( Auth::user()->urlFieldCreate() ) }}" class="btn btn-success" data-toggle="modal" data-target="#crear">Crear</a>
                        @endif
                    </div>
               </form>-->
            </div>
            <!-- Light table -->
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">#</th>
                    <th scope="col" class="sort" data-sort="budget">Cancha</th>
                    <th scope="col" class="sort" data-sort="budget">Info Cancha</th>
                    <th scope="col" class="sort" data-sort="status">Acción</th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach($fields as $field)
      						  <tr>
        							<td>{{$field->id}}</td>
        							<td>
                        <img class="foto" src="{{asset($field->foto)}}" >
                      <td><p>{{$field->name}}<p>
        							    <p>{{$field->status->name}}<p></td>
        							<td>
                        @if($field->status->name == 'Libre')
                        <a href="{{ url( Auth::user()->urlRentalCreate() ) }}" class="btn btn-warning" data-toggle="modal" data-target="#crear">Reservar</a>
                        @endif
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination justify-content-end mb-0">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">
                      <i class="fas fa-angle-left"></i>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#">
                      <i class="fas fa-angle-right"></i>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
          <div class="modal fade" id="crear" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header bg-success">
                    <h5 class="modal-title text-white" >Reserva</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                  	<form action="{{ url( Auth::user()->urlRentalAll())}}" method="post">
                      @csrf
                        <div class="container">
                          <div class="row">
                            <div class="col">
                              <label for="validationCustom01">Fecha</label>
                              <input type="date" name="date"  class="form-control" id="validationCustom01" required>
                              <div class="valid-feedback">
                                Correcto!
                              </div>
                              <div class="invalid-feedback">
                                Complete el recuadro
                              </div>
                            </div>
                            <div class="col">
                              <label >Cancha</label>
                              <select name="field_id"  class="form-control" id="validationCustom01" required>
                                <option value="">Seleccione...</option>
                                  @foreach($fields as $field)
                                    <option value="{{$field->id}}">{{$field->name}}</option>
                                  @endforeach
                              </select>
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Complete el recuadro
                                              </div>
                                          </div>
                          </div>
                                  
                          <div class="row">
                            <div class="col">
                              <label>Hora inicio</label>
                              <input  type="time" name="hora_inicio" class="form-control" id="validationCustom01"  required pattern="">
                              <div class="valid-feedback">
                                Correcto!
                              </div>
                              <div class="invalid-feedback">
                                Complete el recuadro
                              </div>
                            </div>
                            <div class="col">
                              <label >Hora fin:</label>
                              <input  type="time" name="hora_fin" class="form-control" id="validationCustom01" required>
                              <div class="valid-feedback">
                                Correcto!
                              </div>
                              <div class="invalid-feedback">
                                Complete el recuadro
                              </div>
                            </div>
                                        
                           
                          </div><br> 
                           <div class="form-group">
                                <button type="submit" class="btn btn-success float-right">Registrar</button> 
                            </div>
                        </div>     
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <h1 class="title-agile text-center">Registrarse</h1>
        <div class="content-w3ls">
            <div class="content-bottom">
                <form method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="form-group row">
                        <div class="col">
                            <label for="name" class="col-md-4 col-form-label text-white">Nombre:</label>
                            <div class="wthree-field">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col">
                            <label for="lastname" class="col-md-4 col-form-label text-md-right text-white">Apellido: </label>

                            <div class="wthree-field">
                                <input id="lastname" type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" value="{{ old('lastname') }}" required autocomplete="lastname" autofocus>

                                @error('lastname')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="phone" class="col-md-4 col-form-label text-white">Telefono: </label>

                            <div class="wthree-field">
                                <input id="phone" type="number" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                                @error('phone')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col">
                            <label for="document" class="col-md-4 col-form-label text-md-right text-white">Documento: </label>

                            <div class="wthree-field">
                                <input id="document" type="number" class="form-control @error('document') is-invalid @enderror" name="document" value="{{ old('document') }}" required autocomplete="document" autofocus>

                                @error('document')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                            </div>
                        </div>
                    </div>
                       
                    <div class="form-group row">
                            
                        <div class="col">
                            <label for="date" class="col-md-4 col-form-label text-white">F.Nacimiento: </label>

                            <div class="wthree-field">
                                <input id="date" type="date" class="form-control @error('date') is-invalid @enderror" name="date" value="{{ old('date') }}"  max="2002-12-31" required autocomplete="date" autofocus>

                                @error('date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col">
                            <label for="address" class="col-md-4 col-form-label text-white">Dirección: </label>

                            <div class="wthree-field">
                                <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>  

                    <div class="form-group row">
                        <div class="col">
                            <label for="password" class="col-md-4 col-form-label text-white">Contraseña: </label>

                            <div class="wthree-field">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>  
                        </div>
                        <div class="col">
                            <label for="password-confirm" class="col-md-4 col-form-label text-white">Confirmar:</label>

                            <div class="wthree-field">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col">
                            <label for="email" class="col-md-4 col-form-label text-white">Email: </label>

                            <div class="wthree-field">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="wthree-field">
                        <input id="saveForm" name="saveForm" type="submit" value="Registrar" />
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</div>
@endsection

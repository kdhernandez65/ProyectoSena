@extends('layouts.app')
@section('content')
	<section class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
        		<div class="card border-success">
          			<div class="card-header bg-success ">
            			Crear Cancha
          			</div>
          			<div class="card-body ">
            			<form action="{{route('field.store')}}" method="post">
                            @csrf

                            <div class="form-group ">
                                <label class="col-form-label ">Estado</label>
                                <input  type="text" name="status_id" class="form-control ">
                            </div>
            				<div class="form-group ">
            					<label class="col-form-label ">Nombre</label>
                                <input  type="text" name="name" class="form-control ">
            				</div>
            		

            				<div class="form-group ">
            				    <button type="submit" class="btn btn-success full-width">Registrar</button>
                            </div>
            			</form>
          			</div>
        		</div>
            </div>
        </div>
	</section>
@endsection
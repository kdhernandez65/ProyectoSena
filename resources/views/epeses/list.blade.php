@extends('layouts.argon')
@section('content')
	<section class="container">
		<div class="row">
			<h1>Gestión de Canchas</h1>
			<article class="col-md-12 mt-3 ">
                <div class="card">
                    <div class="card-body bg-success">
                        <form action="{{route('eps/show')}}" method="post" novalidate class="form-inline">
                        @csrf
                            <div class="form-group">
                                <label class="mr-2">Nombre </label>
                                <input type="text" name="name" class="form-control" placeholder="Buscar por Nombre">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info ml-2 mr-2">Buscar</button>
                                <a href="{{route('eps.index')}}" class="btn btn-warning">Todo</a>
                                <a href="{{route('eps.create')}}" class="btn btn-dark" data-toggle="modal" data-target="#crear">Crear</a>
                            </div>
                            <div>
                            	
                            </div>
                        </form>
                    </div>
                </div>
            </article>

			<article class="col-md-12">
				<table class="table align-items-center table-flush">
					<thead class="thead-light">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Telefono</th>
							<th>Acción</th>
						</tr>
					</thead>
					<tbody>
						@foreach($epeses as $eps)
						<tr>
							<td>{{$eps->id}}</td>
							<td>{{$eps->name}}</td>
							<td>{{$eps->phone}}</td>
							<td>
								<a class="btn btn-success btn-xs" 
								href="{{route('eps.edit',['ep' => $eps->id])}}">Editar
								</a>
							<a class="btn btn-danger btn-xs" href="{{route('eps/destroy', ['id' => $eps->id])}}">Eliminar</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>

				<div class="modal fade" id="crear" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
				  	<div class=" modal-dialog">
				    	<div class="modal-content">
				      		<div class="modal-header bg-success">
				        		<h5 class="modal-title" id="exampleModalLabel">Crear Eps</h5>
				        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				          		<span aria-hidden="true">&times;</span>
				        		</button>
				      		</div>
						    <div class="modal-body">
						        
						        <form action="{{route('eps.store')}}" method="post">
		                            @csrf

		                            
		            				<div class="form-group ">
		            					<label class="col-form-label ">Nombre</label>
		                                <input  type="text" name="name" class="form-control ">
		            				</div>
		            				<div class="form-group ">
		                                <label class="col-form-label ">Telefono</label>
		                                <input  type="number" name="phone" class="form-control ">
		                            </div>

		            				<div class="form-group ">
		            				    <button type="submit" class="btn btn-success float-right">Registrar</button>
		                            </div>
		            			</form>
						    </div>
				    	</div>
				    </div>
				</div>
			</article>
		</div>
	</section>
@endsection
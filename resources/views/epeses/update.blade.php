@extends('layouts.argon')
@section('content')
    <section class="container">
        <div class="row">
            <article class="col-md-12 mt-3 ">
                    <div class="card">
                        <div class="card-header bg-success text-white">
                            Información Eps
                        </div>
                        <div class="card-body ">
                            <form action="{{route('eps/update', ['id' =>$eps->id ])}}" method="post">
                                @csrf

                                
                                <div class="form-group ">
                                    <label class="col-form-label ">Nombre</label>
                                    <input  type="text" name="name" class="form-control " value="{{$eps->name}}">
                                </div>
                                <div class="form-group ">
                                    <label class="col-form-label ">Telefono</label>
                                    <input  type="text" name="phone" class="form-control"  value="{{$eps->phone}}" >
                                </div>
                                <div class="form-group ">
                                    <button type="submit" class="btn btn-success float-right">Registrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
            </article>
        </div>
    </section>
@endsection
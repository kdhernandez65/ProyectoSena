<ul>	
	<li class="nav-item">
	    <a class="nav-link active" href="{{url('client/reserva')}}">
	      <i class="ni ni-calendar-grid-58 text-primary"></i>
	      <span class="nav-link-text">Reservas</span>
	    </a>
	  </li>
	<li class="nav-item">
	    <a class="nav-link active" href="{{url('client/rental')}}">
	      <i class="ni ni-diamond text-green"></i>
	      <span class="nav-link-text">Reservar cancha</span>
	    </a>
    </li>
	<li class="nav-item">
	    <a class="nav-link" href="{{url( Auth::user()->urlUserEditPerfil(Auth::user()->id) ) }}">
	      <i class="ni ni-circle-08 text-blue"></i>
	      <span class="nav-link-text">Perfil</span>
	    </a>
  	</li>
</ul>
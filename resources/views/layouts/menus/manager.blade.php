<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link active" href="{{url('Manager/reserva')}}">
      <i class="ni ni-calendar-grid-58 text-primary"></i>
      <span class="nav-link-text">Reservas</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('Manager/role')}}">
      <i class="ni ni-badge text-orange"></i>
      <span class="nav-link-text">Roles</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="{{url('Manager/status')}}">
      <i class="ni ni-tv-2 text-primary"></i>
      <span class="nav-link-text">Estados</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url( Auth::user()->urlUserEditPerfil(Auth::user()->id) ) }}">
      <i class="ni ni-circle-08 text-blue"></i>
      <span class="nav-link-text">Perfil</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="{{url('Manager/user')}}">
      <i class="ni ni-single-02 text-green"></i>
      <span class="nav-link-text">Usuarios</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('Manager/field')}}">
      <i class="ni ni-diamond text-info"></i>
      <span class="nav-link-text">Canchas</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" href="{{url('Manager/eps')}}">
      <i class="ni ni-sound-wave text-info"></i>
      <span class="nav-link-text">Eps</span>
    </a>
  </li>
</ul>

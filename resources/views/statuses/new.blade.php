@extends('layouts.app')
@section('content')
	<section class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
        		<div class="card border-success">
          			<div class="card-header bg-success">
            			Crear Estado
          			</div>
          			<div class="card-body ">
            			<form action="{{route('status.store')}}" method="post">
                            @csrf
            				<div class="form-group ">
            					<label class="col-form-label ">Nombre</label>
                                <input  type="text" name="name" class="form-control ">
            			    </div>

            				<div class="form-group ">
            					<label class=" col-form-label ">Descripcion</label>
                                <input  type="text" name="description" class="form-control ">
            				</div>

            				<div class="form-group ">
            				    <button type="submit" class="btn btn-success full-width">Registrar</button>
                            </div>
            			</form>
          			</div>
        		</div>
            </div>
        </div>
	</section>
@endsection
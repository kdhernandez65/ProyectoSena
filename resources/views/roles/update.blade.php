@extends('layouts.argon')
@section('content')
    <div class="main-content" id="panel">
        <div class="header  pb-6">
            <div class="container-fluid ">
                <div class="header-body " >
                    <div class="row align-items-right py-4">
                        <div class="col-lg-6 col-7">
                          <h6 class="h2 text-blck d-inline-block ">Roles</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4  ">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark ">
                              <li class="breadcrumb-item "><a href="{{route('home')}}"><i class="fas fa-home"></i></a></li>
                              <li class="breadcrumb-item "><a href="#">Listado</a></li>
                              <li class="breadcrumb-item active " aria-current="page">Editar</li>
                            </ol>
                          </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid mt--6 ">
            <div class="row ">
                <article class="col-md-12 mt-3">
                    <div class="card ">
                        <div class="card-header bg-success text-white ">
                            Información Rol
                        </div>
                            <div class="card-body ">
                                <form action="{{url( \Auth::user()->urlRoleUpdate($role->id) ) }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" name="name" class="form-control" required value="{{$role->name}}">
                                    </div>
                                
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success float-right">Guardar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </article>
            </div>         
        </div>
    </div>
@endsection
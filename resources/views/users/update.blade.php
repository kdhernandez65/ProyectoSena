@extends('layouts.argon')
@section('content')
	<section class="main-content" id="panel">
        <div class="header  pb-6">
            <div class="container-fluid ">
                <div class="header-body " >
                    <div class="row align-items-right py-4">
                        <div class="col-lg-6 col-7">
                          <h6 class="h2 text-blck d-inline-block ">Usuarios</h6>
                          <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4  ">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark ">
                              <li class="breadcrumb-item "><a href="{{route('home')}}"><i class="fas fa-home"></i></a></li>
                              <li class="breadcrumb-item "><a href="#">Listado</a></li>
                              <li class="breadcrumb-item active " aria-current="page">Editar</li>
                            </ol>
                          </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid mt--6 ">
            <div class="row ">
                <article class="col-md-12 mt-3">
            		<div class="card border-success">
              			<div class="card-header bg-success text-white">
                			Informacion Empleado
              			</div>
              			<div class="card-body ">
                			<form action="{{url( \Auth::user()->urlUserUpdate($user->id) ) }}" method="post">
                                @csrf
                                <div class="container">
                                    <div class="row">
                                            <div class="col">
                                                <label >Nombre</label>
                                                <input type="text" name="name"class="form-control"  class="col" value="{{$user->name}}">
                                            </div>
                                            <div class="col">
                                                <label >Apellido</label>
                                                <input  type="text" class="form-control" name="lastname" class="col" value="{{$user->lastname}}">
                                            </div>
                                        </div>
                                    
                                        <div class="row">
                                            <div class="col">
                                                <label>Telefono</label>
                                                <input  type="number" name="phone" class="form-control" value="{{$user->phone}}">
                                            </div>
                                            <div class="col">
                                                <label >Documento</label>
                                                <input  type="number" name="document"class="form-control"  value="{{$user->document}}" >
                                            </div>
                                            <div class="col">
                                                <label >Fecha de Nacimiento</label>
                                                <input  type="date" name="date"class="form-control"  value="{{$user->date}}">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <label>Dirección</label>
                                                <input  type="text" name="address" class="form-control"  value="{{$user->address}}">
                                            </div>
                                            <div class="col">
                                                <label>Eps:</label>
                                                <select name="eps_id" class="form-control" required>
                                                <option value="">Seleccione...</option>
                                                @foreach($epeses as $eps)
                                                    @if($eps->id == $user->eps_id)
                                                        <option value="{{$eps->id}}" selected>{{$eps->name}}</option>
                                                    @else
                                                        <option value="{{$eps->id}}">{{$eps->name}}</option>
                                                    @endif
                                                @endforeach
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label class="col">Estado</label>
                                                <select name="status_id" class="form-control" required>
                                                <option value="">Seleccione...</option>
                                                @foreach($statuses as $status)
                                                    @if($status->id == $user->status_id)
                                                        <option value="{{$status->id}}" selected>{{$status->name}}</option>
                                                    @else
                                                        <option value="{{$status->id}}">{{$status->name}}</option>
                                                    @endif
                                                @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <label >Email</label>
                                                <input  type="text" name="email" class="form-control" class="col" value="{{$user->email}}">
                                            </div>
                                            <div class="col">
                                                <label class="col">Rol</label>
                                                <select name="role_id" class="form-control" required>
                                                <option value="">Seleccione...</option>
                                                @foreach($roles as $role)
                                                    @if($role->id == $user->role_id)
                                                        <option value="{{$role->id}}" selected>{{$role->name}}</option>
                                                    @else
                                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                                    @endif
                                                @endforeach
                                                </select>
                                            </div>
                                          </div>
                                        </div>
                                        
                                    </div>
                                    <div class="form-group">                                    
                                            <button type="submit" class="btn btn-success float-right ">Actualizar</button>
                                    </div> 
                                </div> 
                			</form>
              			</div>
            		</div>
                </article> 
            </div>
        </div>
	</section>
@endsection
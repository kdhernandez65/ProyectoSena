@extends('layouts.app')
@section('content')
	<section class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
        		<div class="card border-success">
          			<div class="card-header bg-success">
            			Crear Empleado
          			</div>
          			<div class="card-body ">
            			<form action="{{route('user.store')}}" method="post">
                            @csrf
            				<div class="container">
                                <div class="row">
                                    <div class="col">
                                        <label >Nombre</label>
                                        <input type="text" name="name" class="col">
                                    </div>
                                    <div class="col">
                                        <label >Apellido</label>
                                        <input  type="text" name="lastname" class="col">
                                    </div>
                                </div>
                            
                                <div class="row">
                                    <div class="col">
                                        <label>Telefono</label>
                                        <input  type="text" name="phone" >
                                    </div>
                                    <div class="col">
                                        <label >Documento</label>
                                        <input  type="text" name="document" >
                                    </div>
                                    <div class="col">
                                        <label >Fecha de Nacimiento</label>
                                        <input  type="date" name="date" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label>Dirección</label>
                                        <input  type="text" name="address" class="col">
                                    </div>
                                    <div class="col">
                                        <label >Eps</label>
                                        <input  type="text" name="eps_id" class="col" >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <label >Telefono</label>
                                        <input  type="text" name="phone" class="col">
                                    </div>
                                    <div class="col">
                                        <label >Email</label>
                                        <input  type="text" name="email" class="col">
                                    </div>
                                    <div class="col">
                                        <label>Contraseña</label>
                                        <input  type="text" name="password" class="col">
                                    </div>
                                  </div>
                                </div>
                            </div>
            				
            				<div class="row ">
                                <div class="col">
            				      <button type="submit" class="btn btn-success full-width">Registrar</button>
                                </div>
                            </div>
            			</form>
          			</div>
        		</div>
            </div>
        </div>
	</section>
@endsection
@extends('layouts.argon')
@section('content')
<script>

        (function() {
          'use strict';
          window.addEventListener('load', function() {
            
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
</script>

  <div class="main-content" id="panel">
    <div class="header  pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-blck d-inline-block mb-0">Usuarios</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="{{route('home')}}"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Listado</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Gestión</li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid mt--6">
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header border-0">
              <div>
                <!--<h2 class="mb-0">Gestion Usuarios</h3>-->
              </div><br>
              <form action="{{ url( Auth::user()->urlUserShow() ) }}" method="post" novalidate class="form-inline">
                @csrf
                  <div class="form-group">
                    <label class="mr-2">Nombre: </label>
                    <input type="text" name="nombre" class="form-control" placeholder="Buscar por Nombre">
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-info ml-2 mr-2">Buscar</button>
                    <a href="{{ url( Auth::user()->urlUserAll()) }}" class="btn btn-warning">Todos Los usuarios</a>
                    <a href="{{ url( Auth::user()->urlUserCreate() ) }}" class="btn btn-success" data-toggle="modal" data-target="#crear">Crear</a>
                  </div>
              </form>
            </div>
            <!-- Light table -->
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">Nombre</th>
                    <th scope="col" class="sort" data-sort="budget">Apellido</th>
                    <th scope="col" class="sort" data-sort="status">Telefono</th>
                    <th scope="col" class="sort" data-sort="status">Documento</th>
                    <th scope="col" class="sort" data-sort="status">Fecha de nacimiento</th>
                    <th scope="col" class="sort" data-sort="status">Dirección</th>
                    <th scope="col" class="sort" data-sort="status">Email</th>
                    <th scope="col" class="sort" data-sort="status">Eps</th>
                    <th scope="col" class="sort" data-sort="status">Estado</th>
                    <th scope="col" class="sort" data-sort="status">Rol</th>
                    <th scope="col" class="sort" data-sort="status">Acciones</th>
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach($users as $user)
                    <tr>
                      <td>{{$user->name}}</td>
                      <td>{{$user->lastname}}</td>
                      <td>{{$user->phone}}</td>
                      <td>{{$user->document}}</td>
                      <td>{{$user->date}}</td>
                      <td>{{$user->address}}</td>
                      <td>{{$user->email}}</td>
                      <td>{{$user->EPS->name}}</td>
                      <td>{{$user->status->name}}</td>
                      <td>{{$user->role->name}}</td>
                      <td>
                        <a class="btn btn-success btn-xs"  
                        href="{{url( Auth::user()->urlUserEdit($user->id) ) }}">
                        <i class="ni ni-ruler-pencil "></i>
                        </a>
                      
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
            <!-- Card footer -->
            <div class="card-footer py-4">
              <nav aria-label="...">
                <ul class="pagination justify-content-end mb-0">
                  <li class="page-item disabled">
                    <a class="page-link" href="#" tabindex="-1">
                      <i class="fas fa-angle-left"></i>
                      <span class="sr-only">Previous</span>
                    </a>
                  </li>
                  <li class="page-item active">
                    <a class="page-link" href="#">1</a>
                  </li>
                  <li class="page-item">
                    <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item">
                    <a class="page-link" href="#">
                      <i class="fas fa-angle-right"></i>
                      <span class="sr-only">Next</span>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
          <div class="modal fade" id="crear" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header bg-success">
                    <h5 class="modal-title text-white" >Crear Empleado</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form action="{{ url( Auth::user()->urlUserAll())}}" method="post" class="needs-validation" novalidate>
                                  @csrf
                          <div class="container">
                                      <div class="row">
                                          <div class="col">
                                              <label for="validationCustom01">Nombre</label>
                                              <input type="text" name="name"  class="form-control" id="validationCustom01" required>
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Complete el recuadro
                                              </div>
                                          </div>
                                          <div class="col">
                                              <label >Apellido</label>
                                              <input  type="text" name="lastname"  class="form-control" id="validationCustom01" required>
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Complete el recuadro
                                              </div>
                                          </div>
                                      </div>
                                  
                                      <div class="row">
                                          <div class="col">
                                              <label>Telefono</label>
                                              <input  type="number" name="phone" class="form-control" id="validationCustom01"  required pattern="">
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Complete el recuadro
                                              </div>
                                          </div>
                                          <div class="col">
                                              <label >Documento</label>
                                              <input  type="number" name="document" class="form-control" id="validationCustom01" required>
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Complete el recuadro
                                              </div>
                                          </div>
                                          <div class="col">
                                              <label >Fecha de Nacimiento</label>
                                              <input  type="date" name="date" max="2002-12-31" class="form-control" id="validationCustom01" required>
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Por favor proporcione una edad valida
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col">
                                              <label>Dirección</label>
                                              <input  type="text" name="address"  class="form-control" id="validationCustom01" required>
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Complete el recuadro
                                              </div>
                                          </div>
                                          <div class="col">
                                              <label >Eps</label>
                                              <select name="eps_id"  class="form-control" id="validationCustom01" required>
                                              <option value="">Seleccione...</option>
                                              @foreach($epeses as $eps)
                                                <option value="{{$eps->id}}">{{$eps->name}}</option>
                                              @endforeach
                                              </select>
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Complete el recuadro
                                              </div>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col">
                                              <label >Email</label>
                                              <input  type="Email" name="email"  class="form-control" id="validationCustom01" required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z0-9_]{1,5}">
                                              <div class="valid-feedback">
                                                Correcto!
                                              </div>
                                              <div class="invalid-feedback">
                                                Complete el recuadro
                                              </div>
                                          </div>
                                        </div>
                                      </div>
                                  </div>
                                <div class="form-group">
                                <button type="submit" class="btn btn-success float-right">Registrar</button> 
                                </div>      
                        </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
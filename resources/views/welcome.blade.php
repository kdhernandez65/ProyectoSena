<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>

<head>
    <title> Amazu | Cancha de futbol 6 </title>
    <!--/tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="cancha de futbol 6 que presta el servicio de alquiler de lunes a domingo en diferentes horarios" />
    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <script>

        (function() {
          'use strict';
          window.addEventListener('load', function() {
            
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
              form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                  event.preventDefault();
                  event.stopPropagation();
                }
                form.classList.add('was-validated');
              }, false);
            });
          }, false);
        })();
        

    </script>
    
    <!--//tags -->
    <link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />

   
    <script src="{{ asset('js/app.js') }}" defer></script>
    
    <link href="{{ asset('css/style.css')}}" rel="stylesheet" type="text/css" media="all" />
    
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //for bootstrap working -->
    <link href="//fonts.googleapis.com/css?family=Montserrat:200,200i,300,400,400i,500,500i,600,600i,700,700i,800" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic'
        rel='stylesheet' type='text/css'>

    <link rel="shortcut icon"  href="images/icono.svg">
</head>

<body>
    <!-- header_top -->
    <div class="header" id="home">
        <div class="banner_header_top_agile_w3ls">
            <div class="header-top-wthree-agileits">
                <nav class="navbar navbar-default">
                    <div class="navbar-header navbar-left">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                        <h1><a class="navbar-brand" href="index.html"><img src="images/ca.png" class="logo-brand" alt="logo"></a></h1>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="banner_header_top_agile_w3ls">
            <div class="header-top-wthree-agileits">
                <nav class="navbar navbar-default">
                    <div class="navbar-header navbar-left">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                        <h1></h1>
                    </div>
                    <div class="header_left">
                        <ul>
                            <a class="btn btn-success" href="{{ route('login') }}">Ingresar</a>

                        </ul>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">

                        <div class="top_nav_left">
                            <nav class="cl-effect-15" id="cl-effect-15">
                                <ul>
                                    <li><a href="#home" >Inicio</a></li>
                                    <li><a href="#nosotros" >Nosotros</a></li>
                                    <li><a href="#galeria" >Galería</a></li>
                                    <li><a href="#tarifas" >Tarifas</a></li>
                                    <li><a href="#testimonios">Testimonios</a>
                                    <li><a href="#contactenos">Contáctenos</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
            
                    
                </div>
                    <div class="clearfix"></div>
                </nav>
            </div>
        </div>

                </nav>
            </div>
        </div>
        <!-- banner -->
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                <li data-target="#myCarousel" data-slide-to="3" class=""></li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="container">
                        <div class="carousel-caption">
                            <h3>Grandes <span>equipos!</span></h3>
                            <h4>Podrás entrenar el deporte que más te apasiona</h4>
                

                        </div>
                    </div>
                </div>
                <div class="item item2">
                    <div class="container">
                        <div class="carousel-caption">
                            <h3>Queremos <span>verte crecer! </span></h3>
                            <h4>y lo harás en tu deporte favorito</h4>
                
                        </div>
                    </div>
                </div>
                <div class="item item3">
                    <div class="container">
                        <div class="carousel-caption">
                            <h3>Distráete jugando! <span></span></h3>
                            <h4>El tiempo pasará volando</h4>
                            

                        </div>
                    </div>
                </div>
                <div class="item item4">
                    <div class="container">
                        <div class="carousel-caption">
                            <h3>Diversion <span>y deporte! </span></h3>
                            <h4>Ven y disfruta de un momento agradable</h4>
                        
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
            <!-- The Modal -->
        </div>
        <!--//banner -->
        <!--//banner -->

        <!-- Modal1 -->
            <div class="modal fade" id="sesionModal" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

                        <div class="signin-form profile">
                            <h3 class="sign"><img src="images/ca.png" class="logo-brand" alt="logo"></a>Iniciar Sesion</h3>
                            <div class="login-form">
                                <form class="needs-validation" novalidate action="{{route ('login')}}" method="post">
                                    @csrf
                                    <input type="email" name="email"  placeholder="Ingrese Correo" id="validationCustom01" required
                                    pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z0-9_]{1,5}" value="<?php echo isset($error['errorMessage']) ? $error['email'] : '' ?>">
                                    <input type="password" name="password" id="validationCustom02" placeholder="Ingrese contraseña"required>
                                    <div class="tp">
                                        <input type="submit" value="Acceder">
                                    </div>
                                </form>
                            </div>
                            <p><a href="#" data-toggle="modal" data-target="#myModal3"> No tienes una cuenta?</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Modal1 -->
        <!-- Modal2 -->
        <div class="modal fade" id="myModal3" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>    
                        <div class="modal-body">
                            <h3 class="sign"><img src="images/ca.png" class="logo-brand" alt="logo"></a>Registrarse</h3>
                            <form action="{{route('register')}}" method="post"  id="form" class="needs-validation" novalidate>
                                @csrf
                                    <div class="form-group row">
                                        <div class="col">
                                          <label for="validationCustom01">Nombre</label>
                                          <input type="text" name="name" class="form-control" id="validationCustom01" required>
                                          <div class="valid-feedback">
                                            Correcto!
                                          </div>
                                          <div class="invalid-feedback">
                                            Complete el recuadro
                                          </div>
                                        </div>
                                        <div class="col">
                                          <label for="validationCustom02">Apellido</label>
                                          <input type="text" name="lastname" class="form-control" id="validationCustom02" 
                                          required>
                                          <div class="valid-feedback">
                                            Correcto!
                                          </div>
                                          <div class="invalid-feedback">
                                            Complete el recuadro
                                          </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                          <label for="validationCustom03">Documento</label>
                                          <input type="number" name="document" class="form-control" id="validationCustom03" required>
                                         <div class="valid-feedback">
                                            Correcto!
                                          </div>
                                          <div class="invalid-feedback">
                                            Complete el recuadro
                                          </div>
                                        </div>
                                        
                                        <div class="col">
                                          <label for="validationCustom05">Telefono</label>
                                          <input type="number"  name="phone"class="form-control" id="validationCustom05" required pattern=".{10,}">
                                          <div class="valid-feedback">
                                            Correcto!
                                          </div>
                                          <div class="invalid-feedback">
                                            complete el recuadro!
                                          </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                          <label for="validationCustom01">Dirreción</label>
                                          <input type="text" name="address" class="form-control" id="validationCustom01" required>
                                          <div class="valid-feedback">
                                            Correcto!
                                          </div>
                                          <div class="invalid-feedback">
                                            Complete el recuadro
                                          </div>
                                        </div>
                                        <div class="col">
                                          <label for="validationCustom04">Fecha de nacimiento</label>
                                          <input type="date" max="2002-12-31" name="date" class="form-control" id="validationCustom03" required>
                                          <div class="valid-feedback">
                                            Correcto!
                                          </div>
                                          <div class="invalid-feedback">
                                            Por favor proporcione una edad valida
                                          </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                          <label for="validationCustom02">Email</label>
                                          <input type="email" name="email" class="form-control" id="validationCustom02" 
                                          required pattern="[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z0-9_]{1,5}">
                                          <div class="valid-feedback">
                                            Correcto!
                                          </div>
                                          <div class="invalid-feedback">
                                            Complete el recuadro
                                          </div>
                                        </div>
                                        <div class="col">
                                          <label for="validationCustom01">Contraseña</label>
                                          <input type="password" name="password" class="form-control" id="validationCustom01" required pattern=".{8,}">
                                          <div class="valid-feedback">
                                            Correcto!
                                          </div>
                                          <div class="invalid-feedback">
                                            Complete el recuadro
                                          </div>
                                          <p>Debe tener más de 8 caracteres</p>
                                        </div>
                                        <div class="col">
                                            <label for="password-confirm">Confirmar contraseña: </label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required id="validationCustom01">
                                            <div class="valid-feedback">
                                            Correcto!
                                            </div>
                                            <div class="invalid-feedback">
                                                Complete el recuadro
                                            </div>
                                              <p>Debe tener más de 8 caracteres</p>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="form-check">
                                          <input class="form-check-input" type="checkbox" value="" id="invalidCheck" required>
                                          <label class="form-check-label" for="invalidCheck">
                                            Agree to terms and conditions
                                          </label>
                                          <div class="invalid-feedback">
                                            You must agree before submitting.
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-success">
                                                {{ __('Register') }}
                                            </button>
                                         </div>
                                    </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
        <!-- //Modal2 -->
    </div>
    <!--// header_top -->
    <!--about-->
    <div id="nosotros" class="ab_content">
        <div class="container">
            <h3 class="tittle-w3ls">Cancha sintética</h3>
            <div class="about-top">
                <h3 class="subheading">AMAZU(Anibal Martinez Zuleta)</h3>
                <p class="paragraph">Somos una empresa naciente ya que llevamos apenas tres años de existencia.Como todo, este proyecto inicio como un sueño; el sueño de prestar a nuestros clientes un servicio de calidad para disfrutar al maximo la pasion del futbol.  
                </p>
            </div>
            <div class="about-main">
                <div class="col-md-6 about-left">
                </div>

                <div class="col-md-6 about-right">
                    <h3>Nuestros servicios</h3>
                    <p class="paragraph">Por el momento solo contamos co una cancha para el prestamos del servicio. Este deficit de espacios deportivos lo equilibramos prestando el mejor servicio, ya que cumplimos con los estandares de calidad y ofrecemos servicios adicionales como el prestamos de implementos deportivos e hidratacion. </p>

                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="about-main sec">

                <div class="col-md-6 about-right two">
                    <h3>Reservas Empresariales y Torneos</h3>
                    <p class="paragraph">Si quieres organizar un evento empresarial o torneo en el cual necesites un tiempo especifico. Contactanos y cuentanos cual es tu plan para asi poder ayudarte y ofrecerte el paquete que mas se acomode a tus necesidades. </p>

                </div>
                <div class="col-md-6 about-left two">
                </div>

                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
    <!--//about-->
    <!--/works-->
    <div class="works">
        <div class="container">
            <h3 class="tittle-w3ls cen p">POR QUE RESERVAR EN AMAZU</h3>
            <div class="inner_sec_info_w3ls_agile">
                <div class="ser-first">
                    <div class="col-md-3 ser-first-grid text-center">
                        <span class="fa fa-shield" aria-hidden="true"></span>
                        <h3>ESPACIOS AMPLIOS</h3>
                        <p>Nuestros espacios deportivos son amplios para que sientas la mayor libertad a la hora de disfrutar de tu deporte favorito.</p>
                    </div>
                    <div class="col-md-3 ser-first-grid text-center">
                        <span class="fa fa-pencil" aria-hidden="true"></span>
                        <h3>PRESTAMO DE IMPLEMENTOS</h3>
                        <p>Si no tienes balon o una camiseta para diferenciarte de tu rival no te preocupes, nosotros te prestamos lo que necesites para brindarte la mejor experiencia.</p>
                    </div>
                    <div class="col-md-3 ser-first-grid text-center">
                        <span class="fa fa-star" aria-hidden="true"></span>
                        <h3>HIDRATACIÓN</h3>
                        <p>Contamos con un espacio de hidratacion gratuito y tambien con una tienda en la que podrás comprar la bebida que prefieras.</p>
                    </div>
                    <div class="col-md-3 ser-first-grid text-center">
                        <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                        <h3>ILUMINACIÓN</h3>
                        <p>Nuestros espacios estan ubicados estrategicamente para aprovechar la luz natural de la mejor manera durante el día y durante la noche contamos con iluminación artificial profesional.</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
    </div>
    <!--/works-->
    <!-- /portfolio-->
    <div id="galeria" class="portfolio-project">
        <div class="container">
    <h3 class="tittle-w3ls cen">Galería</h3>
            <div class="inner_sec_info_w3ls_agile">
                    <div class="col-md-6 portfolio-grids_left">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/g1.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/g1.jpg" class="img-responsive" alt=" " />
                                <div class="b-wrapper">
                                   <h4>AMAZU</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/g2.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/g2.jpg" class="img-responsive" alt=" " />
                                <div class="b-wrapper">
                                  <h4>AMAZU</h4>
                                    
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/g3.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/g3.jpg" class="img-responsive" alt=" " />
                                <div class="b-wrapper">
                                  <h4>AMAZU</h4>
                                    
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 portfolio-grids sec_img" data-aos="zoom-in">
                        <a href="images/g10.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/g10.jpg" class="img-responsive" alt=" " />
                                <div class="b-wrapper">
                                      <h4>AMAZU</h4>
                                    
                                </div>
                            </a>
                    </div>
                        <div class="col-md-6 portfolio-grids sec_img" data-aos="zoom-in">
                        <a href="images/g11.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/g11.jpg" class="img-responsive" alt=" " />
                                <div class="b-wrapper">
                                      <h4>AMAZU</h4>
                                    
                                </div>
                            </a>
                    </div>
                    <div class="col-md-6 portfolio-grids_left">
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/g5.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/g5.jpg" class="img-responsive" alt=" " />
                                <div class="b-wrapper">
                                      <h4>AMAZU</h4>
                                    
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/g4.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/g4.jpg" class="img-responsive" alt=" " />
                                <div class="b-wrapper">
                                  <h4>AMAZU</h4>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-6 portfolio-grids" data-aos="zoom-in">
                            <a href="images/g6.jpg" class="b-link-stripe b-animate-go lightninBox" data-lb-group="1">
                                <img src="images/g6.jpg" class="img-responsive" alt=" " />
                                <div class="b-wrapper">
                                  <h4>AMAZU</h4>
                                    
                                </div>
                            </a>
                        </div>

                    </div>
                    <div class="clearfix"> </div>
                </div>
        </div>
    </div>

    <!--/ pricing-->
    <div id="tarifas" class="pricing">
        <div class="container">
            <h3 class="tittle-w3ls cen p">Precios del alquiler</h3>
            <div class="inner_sec_info_w3ls_agile">
                <div class="col-md-4 pricing_inner_main">
                    <div class="pricing-top">
                        <h3>Lunes a Viernes</h3>
                        <p>Horarios</p>
                    </div>
                    <div class="pricing-bottom">
                        <div class="pricing-bottom-bottom">
                            <p>7:00am a 12:00pm: $60.000</p>
                            <p>1:00pm a 5:00pm: $80.000</p>
                            <p>6:00pm a 11:00pm: $100.000</p>
                        </div>

                        <div class="sign text-center">
                            <a class="popup-with-zoom-anim" href="#" data-toggle="modal" data-target="#myModal4">RESERVAR</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pricing_inner_main active">
                    <div class="pricing-top orange">
                        <h3>Sabados</h3>
                        <p>Horarios</p>
                    </div>
                    <div class="pricing-bottom">
                        <div class="pricing-bottom-bottom">
                            <p>10:00am a 1:00pm: $80.00</p>
                            <p>3:00pm a 11:00pm: $100.000</p>
                            <p>Promociones</p>
                        </div>

                        <div class="sign text-center">
                            <a class="popup-with-zoom-anim orange active" href="#" data-toggle="modal" data-target="#myModal4">RESERVAR</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pricing_inner_main">
                    <div class="pricing-top purple">
                        <h3>Domingos y festivos</h3>
                        <p>Horarios</p>
                    </div>
                    <div class="pricing-bottom">
                        <div class="pricing-bottom-bottom">
                            <p>8:00am a 12:00pm: $80.000</p>
                            <p>12:00pm a 9:00pm: $90.000</p>
                            <p>9:00pm a 11:00pm: $100.000</p>
                        </div>

                        <div class="sign text-center">
                            <a class="popup-with-zoom-anim purple" href="#" data-toggle="modal" data-target="#myModal4">RESERVAR</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- Popup-Box -->
<!-- Modal2 -->
        <div class="modal fade" id="myModal4" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>

            <div class="pop_up">
                <div class="payment-online-form-left">
                    <form action="#" method="post">
                        <h4>Personal Details</h4>
                        <ul>
                            <li><input type="text" name="First Name" placeholder="First Name" required=""></li>
                            <li><input type="text" name="Last Name" placeholder="Last Name" required=""></li>
                        </ul>
                        <ul>
                            <li><input type="email" class="email" name="Email" placeholder="Email" required=""></li>
                            <li><input type="text" name="Number" placeholder="Mobile Number" required=""></li>
                        </ul>
                        <textarea name="Message" placeholder="Address..." required=""></textarea>
                        <div class="clearfix"></div>

                        <ul class="payment-sendbtns">
                            <li><input type="reset" value="Reset"></li>
                            <li><input type="submit" name="Send" value="Send"></li>
                        </ul>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
    <!-- //Popup-Box -->

    <!--// pricing-->

    <!-- Testimonials -->
    <div id="testimonios" class="testimonials">
        <div class="container">
            <h3 class="tittle-w3ls">Testimonios de nuestros clientes</h3>
            <div class="inner_sec_info_w3ls_agile">
                <div class="col-md-6 testimonials-main">
                        <div class="carousel slide two" data-ride="carousel" id="quote-carousel">
                            <!-- Bottom Carousel Indicators -->
                            <ol class="carousel-indicators two">
                                <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#quote-carousel" data-slide-to="1"></li>
                                <li data-target="#quote-carousel" data-slide-to="2"></li>
                            </ol>

                        <div class="carousel-inner">
                                <div class="item active">

                                    <div class="inner-testimonials">
                                        <img src="images/1.jpg" alt=" " class="img-responsive" />
                                        <div class="teastmonial-info">
                                            <h5>Hector Rodriguez</h5>
                                            <p class="paragraph">"La cancha es genial, es amplia y te sientes libre al moverte. La sensacion del pasto es muy confortante y puesdes jugar por varias horas sin cansarte." </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">

                                    <div class="inner-testimonials">
                                        <img src="images/2.jpg" alt=" " class="img-responsive" />
                                        <div class="teastmonial-info">
                                            <h5>Juan Pablo</h5>
                                            <p class="paragraph">"Mi equipo y yo hemos reservado la cancha cada fin de semana durante un año. Nos fascina jugar en esta cancha y el servicio que nos brindan las personas que trabajan aqui." </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">

                                    <div class="inner-testimonials">
                                        <img src="images/3.jpg" alt=" " class="img-responsive" />
                                        <div class="teastmonial-info">
                                            <h5>Andres Neira</h5>
                                            <p class="paragraph">"Es una cancha muy bien iluminada para poder jugar de noche, esta muy bien ubicada y al aire libre lo que te brinda mayor comodidad al jugar" </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- Carousel Buttons Next/Prev -->
                            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>

                        </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    <!-- //Testimonials -->
    <!-- footer -->
    <footer>
        <div id="contactenos" class="footer-top-w3-agileits">
            <div class="container">
                <div class="footer-grid-wthree-agiles">
                    <div class="col-md-5 footer-grid-wthree-agile">
                        <h3>CONTÁCTANOS</h3>
                        <p>Si necesitas algo o si tienes alguna duda sobre nosotros y nuestros servicios, ingresa tu nombre y tu correo para que nosotros nos pongamos en contacto contigo y tengamos el gusto de atenderte resolver tus dudas.
                        </p>
                        <ul class="footer_list_icons">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 footer-grid-wthree-agile">

                        <ul class="footer-list">
                            <li> <a href="about.html">Nosotros</a> </li>
                            <li> <a href="gallery.html">Galeria</a> </li>
                            <li> <a href="shop.html">Tarifas</a> </li>
                            <li> <a href="blog.html">Contactenos</a> </li>
                        </ul>
                    </div>
                    <div class="col-md-4 footer-grid-wthree-agile">
                        <form action="#" method="post">
                            <input type="text" name="text" placeholder="Nombre" required="">
                            <input type="email" name="Email" placeholder="Email" required="">
                            <input type="submit" value="enviar">
                        </form>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="copy_right">
                    <p>© 2020 cancha AMAZU. Todos los derechos reservados| Design by <a href="http://w3layouts.com/">W3layouts</a></p>
                </div>
            </div>
        </div>

    </footer>
    <!-- //footer -->
    <a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <!-- js -->
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>

    <!--search-bar-->
    <script src="js/search.js"></script>
    <!--//search-bar-->
            
<!-- start-smoth-scrolling -->
    <script type="text/javascript" src="js/move-top.js"></script>
    <script type="text/javascript" src="js/jquery.easing.min.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- Pricing-Popup-Box-JavaScript -->
    <script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
    <!-- //Pricing-Popup-Box-JavaScript -->
    <script>
        $('ul.dropdown-menu li').hover(function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });
    </script>
 <!-- js for portfolio lightbox -->
    <script src="js/jquery.chocolat.js "></script>
    <link rel="stylesheet " href="css/chocolat.css " type="text/css " media="screen ">
    <!--light-box-files -->
    <script type="text/javascript ">
        $(function () {
            $('.portfolio-grids a').Chocolat();
        });
    </script>
    <!-- /js for portfolio lightbox -->

    <!-- smooth-scrolling-of-move-up -->
    <script type="text/javascript">
        $(document).ready(function () {
            /*
            var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
            };
            */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <!-- //smooth-scrolling-of-move-up -->

    <script type="text/javascript" src="js/bootstrap.js"></script>

    <script src="{{asset('js/jquery.js')}}"> </script>
    <script src="{{asset('js/popper.min.js')}}"> </script>
    <script src="{{asset('js/bootstrap.min.js')}}"> </script>
   
</body>

</html>
<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Client Routes
|--------------------------------------------------------------------------
|
| Here is where you can register client routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "client" middleware group. Now create something great!
|
*/


Route::resource('field', 'FieldController');
Route::get('field/destroy/{id}', ['as'=> 'field/destroy', 'uses' => 'FieldController@destroy']);
Route::post('field/show', ['as' => 'field/show', 'uses' => 'FieldController@show']);
Route::post('field/update/{id}', ['as' => 'field/update','uses' => 'FieldController@update']);

Route::resource('rental', 'RentalController');
Route::get('rental/destroy/{id}', ['as'=> 'rental/destroy', 'uses' => 'RentalController@destroy']);
Route::post('rental/show', ['as' => 'rental/show', 'uses' => 'RentalController@show']);
Route::post('rental/update/{id}', ['as' => 'rental/update','uses' => 'RentalController@update']);

Route::resource('perfil', 'PerfilController');
Route::post('perfil/show', ['as' => 'perfil/show', 'uses' => 'PerfilController@show']);
Route::post('perfil/update/{id}', ['as' => 'perfil/update', 'uses' => 'PerfilController@update']);

Route::resource('reserva', 'ReservaController');
Route::get('reserva/destroy/{id}', ['as'=> 'reserva/destroy', 'uses' => 'reservaController@destroy']);
Route::post('reserva/show', ['as' => 'reserva/show', 'uses' => 'ReservaController@show']);
Route::post('reserva/update/{id}', ['as' => 'reserva/update','uses' => 'ReservaController@update']);

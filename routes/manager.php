<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Manager Routes
|--------------------------------------------------------------------------
|
| Here is where you can register manager routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "manager" middleware group. Now create something great!
|
*/



Route::resource('status', 'StatusController');
Route::get('status/destroy/{id}', ['as'=> 'status/destroy', 'uses' => 'StatusController@destroy']);
Route::post('status/show', ['as' => 'status/show', 'uses' => 'StatusController@show']);
Route::post('status/update/{id}', ['as' => 'status/update','uses' => 'StatusController@update']);


Route::resource('role', 'RoleController');
Route::get('role/destroy/{id}', ['as'=> 'role/destroy', 'uses' => 'RoleController@destroy']);
Route::post('role/show', ['as' => 'role/show', 'uses' => 'RoleController@show']);
Route::post('role/update/{id}', ['as' => 'role/update','uses' => 'RoleController@update']);


Route::resource('field', 'FieldController');
Route::get('field/destroy/{id}', ['as'=> 'field/destroy', 'uses' => 'FieldController@destroy']);
Route::post('field/show', ['as' => 'field/show', 'uses' => 'FieldController@show']);
Route::post('field/update/{id}', ['as' => 'field/update','uses' => 'FieldController@update']);

Route::resource('user', 'UserController');
Route::get('user/destroy/{id}', ['as'=> 'user/destroy', 'uses' => 'UserController@destroy']);
Route::post('user/show', ['as' => 'user/show', 'uses' => 'UserController@show']);
Route::post('user/update/{id}', ['as' => 'user/update', 'uses' => 'UserController@update']);

Route::resource('eps', 'EpsController');
Route::get('eps/destroy/{id}', ['as'=> 'eps/destroy', 'uses' => 'EpsController@destroy']);
Route::post('eps/show', ['as' => 'eps/show', 'uses' => 'EpsController@show']);
Route::post('eps/update/{id}', ['as' => 'eps/update','uses' => 'EpsController@update']);

Route::resource('perfil', 'PerfilController');
Route::post('perfil/show', ['as' => 'perfil/show', 'uses' => 'PerfilController@show']);
Route::post('perfil/update/{id}', ['as' => 'perfil/update', 'uses' => 'PerfilController@update']);


Route::resource('rental', 'RentalController');
Route::get('rental/destroy/{id}', ['as'=> 'rental/destroy', 'uses' => 'RentalController@destroy']);
Route::post('rental/show', ['as' => 'rental/show', 'uses' => 'RentalController@show']);
Route::post('rental/update/{id}', ['as' => 'rental/update','uses' => 'RentalController@update']);


Route::resource('reserva', 'ReservaController');
Route::get('reserva/destroy/{id}', ['as'=> 'reserva/destroy', 'uses' => 'reservaController@destroy']);
Route::post('reserva/show', ['as' => 'reserva/show', 'uses' => 'ReservaController@show']);
Route::post('reserva/update/{id}', ['as' => 'reserva/update','uses' => 'ReservaController@update']);


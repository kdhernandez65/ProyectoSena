<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelantionships extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table)
        {
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('cascade');
            $table->foreign('eps_id')->references('id')->on('e_p_s')->onUpdate('cascade');
            $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade');

        });

        Schema::table('rentals', function($table)
        {
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('cascade');
            $table->foreign('field_id')->references('id')->on('fields')->onUpdate('cascade');
            $table->foreign('price_id')->references('id')->on('prices')->onUpdate('cascade');

        });

        Schema::table('fields', function($table)
        {
            
            $table->foreign('status_id')->references('id')->on('statuses')->onUpdate('cascade');

        });


        



        /*Schema::table('statuses', function ($table) {
            $table->foreign('type_status_id')->references('id')->on('type_statuses')->onUpdate('cascade');
        });*/


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relantionships');
    }
}
